class Particle:
    def __init__(self, x=0.0, velocity_mps=0.0):
        self.x = x
        self.velocity_mps = velocity_mps
        print(
            "Initialized a particle at x:", self.x, "velocity:", 
            self.velocity_mps)

def main():
    particle1 = Particle(0.5, 2.0)
    particle2 = Particle(1.0, 3.0)

if __name__ == "__main__":
    main()
