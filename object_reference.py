class Particle:
    def __init__(self, x):
        self.x = x
        
particle1 = Particle(20)
print("particle1.x:", particle1.x)

particle2 = particle1
particle2.x = 999

print("particle1.x:", particle1.x)
print("particle2.x:", particle2.x)
print(particle1)
print(particle2)
