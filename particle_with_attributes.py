class Particle:
    pass
    
particle1 = Particle()
particle2 = Particle()

particle1.x = 0.5
particle1.velocity_mps = 2.0
print(
    "Initialized a particle at x:", particle1.x, "velocity:", 
    particle1.velocity_mps)

particle2.x = 1.0
particle2.velocity_mps = 3.0
print(
    "Initialized a particle at x:", particle2.x, "velocity:", 
    particle2.velocity_mps)

