import random

SIMULATION_DURATION_SECONDS = 5.0
TIMESTEP_SECONDS = 1.0
N_PARTICLES = 5
MAX_SPEED = 5
START_LOCATION = 0.0

class Particle:
    def __init__(self, number, x=0.0, velocity_mps=0.0):
        self.number = number
        self.x = x
        self.velocity_mps = velocity_mps
        print(
            "Initialized particle:", number," at x:", self.x, " velocity_mps:", 
            self.velocity_mps)
        
    def move(self, delta_seconds):
        self.x += delta_seconds * self.velocity_mps
        
    def print_location(self):
        print("Particle: ", self.number, "x:", self.x)


def main():
    particles = create_particles(N_PARTICLES, START_LOCATION, MAX_SPEED)
    simulate(particles, TIMESTEP_SECONDS, SIMULATION_DURATION_SECONDS)
    
    
def create_particles(n_particles, start_location, max_speed):
    particles = []
    for i in range(n_particles):
        new_particle = Particle(i, start_location, random.random()* max_speed)
        particles.append(new_particle)
    
    return particles
    
    
def simulate(particles, delta_seconds, duration_seconds):
    time = 0.0
    while time < duration_seconds:
        print("****Timestep at time:", time, "****")
        timestep(particles, delta_seconds)
        time += delta_seconds
        
        
def timestep(particles, delta_seconds):
    for particle in particles:
        particle.move(delta_seconds)
        particle.print_location()
        
        
if __name__ == "__main__":
    main()
