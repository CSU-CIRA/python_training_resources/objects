TIMESTEP_SECONDS = 1.0

class Particle:
    def __init__(self, x=0.0, velocity_mps=0.0):
        self.x = x
        self.velocity_mps = velocity_mps
        print(
            "Initialized a particle at x:", self.x, " velocity_mps:", 
            self.velocity_mps)
        
    def move(self, delta_seconds):
        self.x += delta_seconds * self.velocity_mps
        
    def print_location(self):
        print("x: ", self.x)

def main():
    particle1 = Particle(0.5, 2.0)
    particle2 = Particle(1.0, 3.0)
    
    particle1.move(TIMESTEP_SECONDS)
    particle1.print_location()
    
    particle2.move(TIMESTEP_SECONDS)
    particle2.print_location()

if __name__ == "__main__":
    main()
