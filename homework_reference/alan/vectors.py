""" Final layout to cover the various aspects of the homework tasks
"""

from random import random
import math


class Vector2D:
    """Simple class to contain x,y component of a vector and normlize components 
    if requested
    """
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    def __str__(self, ) -> str:
        return f"x={self.x:3.2f}, y={self.y:3.2f}, magnitude={self.magnitude:3.2f}"

    @property
    def magnitude(self, ) -> float:
        """ Calculate the magnitude of the vector from the internal components

        Returns
        -------
        float
            magnitude of vector
        """
        return math.sqrt(self.x**2 + self.y**2)

    def normalize(self, ):
        """ Normalizes components to a magnitude of 1
        """
        mag = self.magnitude
        self.x /= mag
        self.y /= mag


def demo_vectors(nvector):
    vectors = [Vector2D(random(), random()) for i in range(nvector)]

    for vector in vectors:
        print(vector)

    print("normalizing")
    for vector in vectors:
        vector.normalize()

    for vector in vectors:
        print(vector)


if __name__ == "__main__":
    demo_vectors(10)