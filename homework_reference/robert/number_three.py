import math

class Vector2D:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        
    def get_magnitude(self):
        return math.hypot(self.x, self.y)

def main():
    vec1 = Vector2D(0.5, 0.25)
    vec2 = Vector2D(0.1, 0.3)

    print(vec1.x, " ", vec1.y, " ", vec1.get_magnitude())
    print(vec2.x, " ", vec2.y, " ", vec2.get_magnitude())

if __name__ == "__main__":
    main():
