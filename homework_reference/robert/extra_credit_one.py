import math
import random

class Vector2D:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        
    def get_magnitude(self):
        return math.hypot(self.x, self.y)
        
    def normalize(self):
        magnitude = self.get_magnitude()
        self.x /= magnitude
        self.y /= magnitude
        
    def __str__(self):
        magnitude = self.get_magnitude()
        return f"X:{self.x:3.2f} Y:{self.y:3.2f} Magnitude:{magnitude:3.2f}"


def main():
    vectors = create_vectors()
    print_vectors(vectors)
    
    
def create_vectors():
    vectors = []
    for i in range(10):
        new_vector = Vector2D(random.random(), random.random())
        vectors.append(new_vector)
    return vectors
        
        
def print_vectors(vectors):
    for vector in vectors:
        print(str(vector))


if __name__ == "__main__":
    main()
