import math

class Vector2D:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        
    def get_magnitude(self):
        return math.hypot(self.x, self.y)
        
    def normalize(self):
        magnitude = self.get_magnitude()
        self.x /= magnitude
        self.y /= magnitude
        

def main():
    vec1 = Vector2D(0.5, 0.25)
    vec2 = Vector2D(0.1, 0.3)

    print(vec1.x, " ", vec1.y, " ", vec1.get_magnitude())
    print(vec2.x, " ", vec2.y, " ", vec2.get_magnitude())
    
    side_effect(vec1)
    side_effect(vec2)
    print(vec1.get_magnitude())
    print(vec2.get_magnitude())
    
    
def side_effect(vector):
    vector.normalize()

if __name__ == "__main__":
    main()
